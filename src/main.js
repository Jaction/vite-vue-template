import { createApp } from "vue";
import App from "./App.vue";
import router from "./router/index";
import store from './store'
import './assets/styles/tailwind.css'
import 'styles/index.scss'

const app = createApp(App)
app.use(store)
app.use(router)
app.mount("#app");
