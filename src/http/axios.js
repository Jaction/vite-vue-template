import axios from "axios";

const Axios = axios.create({
    baseURL: "", // import.meta.env.VITE_APP_HTTP_URL
    timeout: 10000,
    responseType: "json",
    withCredentials: true, // 是否允许带cookie这些
    headers: {
        "Content-Type": "application/json;charset=UTF-8"
    }
});

//POST传参序列化(添加请求拦截器)
Axios.interceptors.request.use(
    config => {
        // config.headers.token = getCookie("token");
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);


//返回状态判断(添加响应拦截器)
Axios.interceptors.response.use(
    res => {
        return res;
    },
    error => {
        return Promise.reject(error);
    }
);

export default {
    // 对axios的实例重新封装成一个plugin ,方便 Vue.use(xxxx)
    install(Vue, Option) {
        Object.defineProperty(Vue.prototype, "$http", {
            value: Axios
        });
    },
    //Post  请求方式
    post(url, params = {}, config = {}) {
        return new Promise((resolve, reject) => {
            Axios.post(url, params, config).then(response => {
                    resolve(response.data);
                }, err => {
                    reject(err);
                })
                .catch((error) => {
                    reject(error)
                })
        })
    },
    //GET 请求方式
    get(url, params = {}) {
        return new Promise((resolve, reject) => {
            Axios.get(url, {
                    params: params
                }).then(response => {
                    resolve(response.data);
                }, err => {
                    reject(err);
                })
                .catch((error) => {
                    reject(error)
                });
        })

    },

}
