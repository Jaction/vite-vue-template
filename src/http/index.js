import Axios from './axios'

const login = params => Axios.post('/api/login', params);
const createUser = params => Axios.post('/api/createUser', params);


export default {
    login,
    createUser
}