import {
	defineConfig
} from 'vite'
import vue from '@vitejs/plugin-vue'
import { viteMockServe } from 'vite-plugin-mock';
const path = require('path')

// https://vitejs.dev/config/
export default defineConfig({
	resolve: {
		alias: {
			// 键必须以斜线开始和结束
			"@": path.resolve(__dirname, "./src"),
			"http": path.resolve(__dirname, "./src/http"),
			"styles": path.resolve(__dirname, "./src/assets/styles"),
		}
	},
	base: './',
	outDir: 'dist',
	plugins: [vue(), viteMockServe({ supportTs: false })]
})
